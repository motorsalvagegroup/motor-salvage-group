Motor Salvage Group is the UK's easiest car scrappage service. We want our customers to get the best deal. What we quote you is what we pay you. If you are in our service area we can collect from your chosen location and you can have cash the same day!

Address: Unit 1A, Mt Alexander, Camaghael Rd, Fort William PH33 7NF, United Kingdom

Phone: +44 7946 135229

Website: https://motorsalvagegroup.co.uk
